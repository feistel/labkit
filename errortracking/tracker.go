package errortracking

import "github.com/getsentry/sentry-go"

// Tracker is an abstraction for error tracking.
type Tracker interface {
	// Capture reports an error to the error reporting service
	Capture(err error, opts ...CaptureOption)
}

var (
	defaultTracker = newSentryTracker(sentry.CurrentHub())
)

// DefaultTracker returns the default global error tracker.
func DefaultTracker() Tracker {
	return defaultTracker
}

// NewTracker constructs a new Tracker with the provided options.
func NewTracker(opts ...TrackerOption) (Tracker, error) {
	client, err := sentry.NewClient(trackerOptionsToSentryClientOptions(opts...))
	if err != nil {
		return nil, err
	}
	hub := sentry.NewHub(client, sentry.NewScope())
	return newSentryTracker(hub), nil
}

func trackerOptionsToSentryClientOptions(opts ...TrackerOption) sentry.ClientOptions {
	config := applyTrackerOptions(opts)

	return sentry.ClientOptions{
		Dsn:         config.sentryDSN,
		Release:     config.version,
		Environment: config.sentryEnvironment,
	}
}
